/**
The main purpose of this class is just to require another compilation unit.
This means another rule in the makefile (with another object file as output).
*/

class Dummy {
public:
    Dummy(int number) : _num(number) {}
    int getNum();
private:
    int _num;
 };
