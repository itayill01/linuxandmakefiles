# This is our makefile. Lines starting with '#' are comments.
# This file guides the 'make' program how to compile our project,
# and what commands should be ran in order to compile it.

# Each rule is of the following form:
# target: prerequisites 
#	recipe
#	...

# Few important notes:
# - If a prerequisite has been changed, it target will be remade.
# - Each recipe should be indented using tabs ONLY. make does not
#   know how to work with spaces.
# - If a prerequisite does not exists or out of date - it is remade 
#   before it's target.

# with that in mind - let's dive right in.

# By opening the man page (using the 'man' command, shorthand for manual) 
# and by going back to our first year, when we learn the stage of compilation,
# we can make sense out of this rules.

# This rule is the linking stage. The g++ (gnu compile for c++) can has many 
# functions. It contains the linker, compiler and our preprocessor. It also
# contains some other functionalities - such as assembler, and other compiler
# for other languages.

# By default g++ uses the linker, known as ld (shorthand for LoaDer). The linker takes object files as input and
# outputs an binary file. EXE (EXEcutable) file in windows, ELF (Executable 
# Linkable Format) in linux.
target.exe: main.o dummy.o
	g++ main.o dummy.o -o target.exe

# If the '-c' file is specified, the linker is not called. This leaves us with
# The compiler, known as cc (C compiler) and the preprocessor, 
# known as cpp (C PreProcessor, not to be confused with the language).
# Running the compiler outputs an object file, but first the preprocessor is ran, 
# but no file is made output, it is passed directly to the compiler.
main.o: main.cpp
	g++ main.cpp -c -o main.o

dummy.o: dummy.cpp
	g++ dummy.cpp -c -o dummy.o
