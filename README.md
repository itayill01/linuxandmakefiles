# Linux and GNU-Make

This project will guide you, step by step, on how to create you on project in linux (or any other OS), and how to compile it.
Compilation is not just about output some binary, it's much more.
Compilation time should be short, the compilation should be efficient, and by extension - generic.

## Non visual-studio IDEs

Not all IDEs (integrated development environment) will compile your code for you. On linux, and it's distributions, there not many visual-studio like IDE (for editing AND compiling you project), and most of them are not free. But fear not, there are alternatives, and some very good ones.

After finding an editor that suites you well (sublime, vim, vscode), we can 

## Introducing GNU-Make

By just writigin some wierd configuration file, `make` will know how to compile you project.

### Installation

```sh
$ apt install make
```
(It is implicit that priviledged user is required so just run with sudo - it is the linux equivilant of running as admin in windows)

### Documentation
[Make documentation] - Feel free to read more!

### Compilation

After configuring our makefile, we can now run make.
```sh
$ make
```
And ta-dah, we have compiled our code.

[Make documentation]: <https://www.gnu.org/software/make/manual/html_node/index.html#Top>

